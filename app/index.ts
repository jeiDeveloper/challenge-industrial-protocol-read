
import { ListI } from "./interfaces/app.interface";
import OptimizeMemoryCost from "./functions/optimizeMemoryCost"

const inputList: Array<ListI> = [
    {start: 1, length: 6, callback: (error, payload) => {
        console.log({payload})
    }},
    {start: 6, length: 2, callback: (error, payload) => {
        console.log({payload})
    }},
    {start: 11, length: 10, callback: (error, payload) => {
        console.log({payload})
    }},
    {start: 15, length: 2, callback: (error, payload) => {
        console.log({payload})
    }},
    {start: 15, length: 2, callback: (error, payload) => {
        console.log({payload})
    }},
    {start: 20, length: 2, callback: (error, payload) => {
        console.log({payload})
    }},
    {start: 25, length: 2, callback: (error, payload) => {
        console.log({payload})
    }},
    {start: 25, length: 2, callback: (error, payload) => {
        console.log({payload})
    }},
    {start: 25, length: 2, callback: (error, payload) => {
        console.log({payload})
    }},
    {start: 25, length: 2, callback: (error, payload) => {
        console.log({payload})
    }},
];

const optimizeMemoryCost = new OptimizeMemoryCost(inputList);

optimizeMemoryCost.read();

console.log(optimizeMemoryCost.callRead)
import industrialProtocol from "./industrialProtocol";
import { ListI, ListObjectI, FormatReturnI, ListBufferReadI } from "../interfaces/app.interface";

class OptimizeMemoryCost{
    private list: Array<ListI>;
    private listObject: ListObjectI = {}
    private listBufferRead: ListBufferReadI = {};
    private readonly limitLength = 10;
    public callRead = 0;

    constructor(list: Array<ListI>){
        this.list = list;
    }

    format(): FormatReturnI{
        const format = this.list.reduce((a: any, b, z) => {
            const index = (b.start);
            const end = (index + (b.length - 1));
            
            for (let i = index; i <= end; i++) {
                const _i = Math.ceil(i / this.limitLength)
    
                if(!a[_i]) a[_i] = { list: [] }
                if(!this.listObject[z]) this.listObject[z] = [];
    
                a[_i].list.push(i);
                this.listObject[z].push(i);
            }
    
            return a;
        }, {});

        Object.keys(format).forEach((key) => {
            const _key = parseInt(key);
            
            // Sort values ​​from smallest to largest
            format[_key].list.sort((a: number, b: number) => a-b);
    
            // Clean up repeating numbers
            format[_key].list = format[_key].list.reduce((a: Array<number>, b: number) => {
                if(!a.includes(b)) a.push(b);
                return a;
            }, []);
    
            // Split calls to avoid calls to uncalled memory space
            let _i = 0;
            format[_key].listReady = format[_key].list.reduce((a: Array<Array<number>>, b: number) => {
                if(!a[_i]) a[_i] = [];
    
                const prev = a[_i][a[_i].length - 1];
                if(typeof prev === "number" && a[_i][a[_i].length - 1] != (b - 1)) {
                    _i += 1;
                    a[_i] = []
                }
                a[_i].push(b);
                return a;
            }, []);
        });

        return format;
    }

    read(){
        const format = this.format();

        Object.keys(format).forEach(key => {
            format[parseInt(key)].listReady.forEach(array => {
                let start = array[0] -1;
                let end = array.length;

                industrialProtocol.read(start, end, (error, payload) => {
                    this.callRead += 1;
                    if(error) return console.log("An error occurred while trying to read the protocol buffer");
                    payload?.forEach((buffer, index) => {
                        this.listBufferRead[start + index] = buffer;
                    });
                })
            })
        });

        Object.values(this.listObject).forEach((items: Array<number>, index: number) => {
            let data = items.reduce((a: Array<Buffer>, b: number) => {
                a.push(this.listBufferRead[b-1])
                return a;
            }, []);
            
            this.list[index].callback(null, data);
        });
    }
}

export default OptimizeMemoryCost;
import { IndustrialProtocolCallbackI, MemoryProtocolI } from "../interfaces/industrialProtocol.interface";

class IndustrialProtocol {
    private readonly memory: MemoryProtocolI = {};
    private readonly lengthMemory: number = 4096;

    constructor(){
        this.initMemory();
    }

    protected initMemory(){
        for (let i = 0; i <= this.lengthMemory; i++) {
            const buffer = Buffer.from(`${i}`, "binary");
            this.memory[i] = buffer;
        }
    }

    public length(): Number{
        return Object.keys(this.memory).length;
    }

    public read(start: number, length: number, callback: IndustrialProtocolCallbackI){
        if(length > 10) return callback({message: "Length exceeds maximum amount"});
        if(!this.memory[start]) return callback({message: "Invalid memory"});

        let end = start + length;
        let memory = Object.keys(this.memory).slice(start, start + length);
        let data = memory.reduce((store, key) => {
            if(parseInt(key) <= end){
                //@ts-ignore
                store.push(this.memory[key]);
            }
            return store;
        }, []);

        return callback(null, data);
    }
}

export default new IndustrialProtocol();
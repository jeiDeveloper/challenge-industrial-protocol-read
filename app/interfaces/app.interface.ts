import { IndustrialProtocolCallbackI } from "./industrialProtocol.interface";

export interface ListI{
    start: number
    length: number
    callback: IndustrialProtocolCallbackI
}

export interface ListObjectI{
    [key: number]: Array<number>
}

export interface ListBufferReadI{
    [key: number]: Buffer
}

export interface FormatReturnDataI{
    list: Array<number>
    listReady: Array<Array<number>>
}

export interface FormatReturnI{
    [key: number]: FormatReturnDataI
}
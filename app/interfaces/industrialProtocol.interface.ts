export interface ErrorI{
    message: string
}

export interface IndustrialProtocolCallbackI{
    (error?: ErrorI | null, payload?: Array<Buffer> | null): void
}

export interface MemoryProtocolI{
    [key: number]: Buffer
}
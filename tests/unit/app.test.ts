import { ListI } from "../../app/interfaces/app.interface";
import OptimizeMemoryCost from "../../app/functions/optimizeMemoryCost"

describe('Check read optimization', () => {
    const list1: Array<ListI> = [
        {start: 2, length: 9, callback: (error, payload) => {
            console.log("list 1", {payload})
        }},
        {start: 4, length: 2, callback: (error, payload) => {
            console.log("list 1", {payload})
        }},
        {start: 5, length: 1, callback: (error, payload) => {
            console.log("list 1", {payload})
        }},
    ];

    const list2: Array<ListI> = [
        {start: 2, length: 9, callback: (error, payload) => {
            console.log("list 2", {payload})
        }},
        {start: 4, length: 2, callback: (error, payload) => {
            console.log("list 2", {payload})
        }},
        {start: 5, length: 1, callback: (error, payload) => {
            console.log("list 2", {payload})
        }},
        {start: 1, length: 4, callback: (error, payload) => {
            console.log("list 2", {payload})
        }},
        {start: 5, length: 3, callback: (error, payload) => {
            console.log("list 2", {payload})
        }},
        {start: 9, length: 1, callback: (error, payload) => {
            console.log("list 2", {payload})
        }},
        // {start: 15, length: 1, callback: (error, payload) => { //Check that it adds another in the range it makes another necessary call.
        //     console.log("list 2", {payload})
        // }},
    ];

    it("Verify that the two lists are different in size", function () {
        expect(list2.length).toBeGreaterThan(list1.length);
    });
    
    it("Verify that the same number of calls are made to the read function even though list two is greater than 1", function () {
        const optimizeMemoryCost1 = new OptimizeMemoryCost(list1);
        const optimizeMemoryCost2 = new OptimizeMemoryCost(list2);

        optimizeMemoryCost1.read();
        optimizeMemoryCost2.read();

        expect(optimizeMemoryCost1.callRead).toEqual(optimizeMemoryCost2.callRead);
    });
});